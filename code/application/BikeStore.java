package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Specialized", 24, 45);
        bikes[1] = new Bicycle("Orbea", 16, 38);
        bikes[2] = new Bicycle("Yeti", 30, 55);
        bikes[3] = new Bicycle("Niner", 20, 42);

        for(Bicycle bike : bikes) {
            System.out.println(bike);
        }
    }
}